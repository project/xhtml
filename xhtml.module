<?php /* -*- mode: php; indent-tabs-mode: nil; tab-width: 2; -*- */

/**
 * @file
 * The xhtml module's purpose is to aid in coaxing a drupal
 * installation to actually request browsers to interpret its output
 * as XHTML.
 *
 * The current drupal installs uses the silly (but more reliable)
 * method of making sure that all of its output is XHTML-compliant but
 * serving it with a Content-Type of text/html. It even goes so far as
 * to use a <meta http-equiv /> block to enforce this (overriding the
 * actual HTTP Content-Type headers).
 *
 * The result is that browsers will not do much validation on drupal's
 * output. Such validation and error-checking is useful during
 * module-development, as many browsers will give instant feedback or
 * even completely skip rendering a page if it is served as
 * application/xhtml+xml but is not well-formed XML. That is not to
 * say that browsers actually validate the XML -- use
 * http://validator.w3.org/. But, again, actually serving drupal's
 * XHTML with the proper Content-Type does make the validator more
 * happy (and I suspect slightly more strict ;-)).
 *
 * This module has the option to unconditionally force sending output
 * as XHTML (which breaks anything older than IE9). (Yes, I only
 * mentioned IE becuase that's really the only browser for which there
 * are significantly many users who stick with ancient releases).
 */

/**
 * Implement hook_menu() mostly for the administration pages.
 */
function xhtml_menu() {
  return array(
    'admin/settings/xhtml' => array(
      'title' => 'xhtml',
      'description' => 'Configure the Content-Type used to deliver pages to user-agents',
      'file' => 'xhtml.admin.inc',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('xhtml_admin_settings'),
      'access arguments' => array('administer site configuration'),
    ),
  );
}

/**
 * Determine if the client should recieve application/xhtml+xml.
 *
 * The user may have configured that XHTML is served unconditionally
 * or conditionally on the client's Accept: header.
 *
 * The user may have also defined certain path patterns for which
 * XHTML is never served (even if it is otherwise served
 * unconditionally).
 *
 * The definition of a browser interested in XHTML is a browser that
 * satisfies ones of two criteria. The first is, of course, if the
 * browser has application/xhtml+xml at all in its Accept: header. The
 * second is the case where the browser sends no Accept: header at all
 * (as is the case for the W3C validator http://validator.w3.org/) or
 * a header which doesn't explicitly request text/html (which is what
 * drupal would serve).
 */
function _xhtml_use_xhtml() {
  $acceptable = &drupal_static('xhtml_is_acceptable');

  if ($acceptable === NULL) {
    $acceptable = !in_array(TRUE, module_invoke_all('xhtml_disable'));
  }

  return $acceptable;
}

/**
 * Implement hook_xhtml_disable() to respond to the user's settings.
 */
function xhtml_xhtml_disable() {

  if (drupal_match_path(drupal_get_path_alias(), variable_get('xhtml_html_pages', ''))) {
    return TRUE;
  }

  /* assignment is intentional... */
  if (!variable_get('xhtml_test_accept', TRUE)) {
    return FALSE;
  }

  /*
   * I could not find any drupal abstraction around apache2's
   * $_SERVER['HTTP_ACCEPT']. Please file bugs with suggestions ;-).
   */
  if (empty($_SERVER['HTTP_ACCEPT'])
      || strpos($_SERVER['HTTP_ACCEPT'], 'application/xhtml+xml') !== FALSE
      || strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === FALSE) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Implement hook_init() to touch the HTTP headers.
 */
function xhtml_init() {
  if (!_xhtml_use_xhtml()) {
    return;
  }
  drupal_add_http_header('Content-Type', 'application/xhtml+xml; encoding=utf-8');
}

/**
 * Implement hook_theme() to override system.module's 'html' theme
 * implementation.
 */
function xhtml_theme($existing, $type, $theme, $path) {
  /*
   * We override the 'html' theme, hook_theme()'s documentation
   * suggests to make use of the values available in $existing.
   *
   * Is this safe from modules' hook_theme() implementations being
   * called in a potentially randomized order?
   */
  $theme_html = array('template' => 'xhtml');
  foreach (array('variables', 'render element', 'pattern') as $key) {
    if (isset($existing['html'][$key])) {
      $theme_html[$key] = $existing['html'][$key];
    }
  }

  return array(
    'html' => $theme_html,
  );
}

/**
 * Implement hook_preprocess() to allow dynamic enabling/disabling of
 * XHTML delivery.
 */
function xhtml_preprocess_html(&$vars) {
  if (_xhtml_use_xhtml()) {
    $vars['xml_shebang'] = '<?xml version="1.0" encoding="utf-8"?>' . PHP_EOL;
  } else {
    unset($vars['xml_shebang']);
  }
}

/**
 * Implement hook_html_head_alter() to remove the meta http-equiv tag
 * which causes browsers to not treat the content as XHTML.
 *
 * Drupal by default forces the following tag:
 * <code><meta http-equiv="Content-Type" content="text/html; encoding=utf-8" /></code>
 * This fixes it to support XHTML properly.
 *
 * \see _drupal_default_html_head()
 * \see drupal_add_html_head()
 */
function xhtml_html_head_alter(&$head_elements) {
  if (!_xhtml_use_xhtml()) {
    return;
  }

  unset($head_elements['system_meta_content_type']);
}
